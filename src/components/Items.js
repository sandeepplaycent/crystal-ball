import React, { useState, useEffect } from 'react'
/*import Celebration from "./confetti"*/


import config from "visual-config-exposer"
import Ball from './Ball'


export default function Items() {
    const width=100
    const height=100
    const ItemArray=config.settings.items

    var length=Object.keys(ItemArray).length-1
    var [state, setState] = useState(length)
    
   
    const RewardImage=Object.entries(ItemArray)[state][1].image
    console.log(state)
    const label=Object.entries(ItemArray)[state][1].label
    const probabilit=Object.entries(ItemArray)[state][1].probability
    

   useEffect(() => {
   
    

       var color =config.settings.titleColor;
       var font = config.settings.titleFont;
    document.getElementsByTagName('h1')[0].style.color=color;
    document.getElementsByTagName('h1')[0].style.fontFamily=font;
    document.getElementsByTagName('h3')[0].style.fontFamily=font;


    /*console.log(ItemArray.length)*/   
    var count=0
    var add=0

var element = document.createElement("link");
element.setAttribute("rel", "stylesheet");
element.setAttribute("type", "text/css");
element.setAttribute("href", "https://fonts.googleapis.com/css?family="+font);
document.getElementsByTagName("head")[0].appendChild(element)
var num = Math.random().toFixed(2)


       var hello=Object.entries(ItemArray)[count][1].probability

     
       if((num>0) && (num <0.10)){
setState(1)
       }
      if((num > 0.20) && (num <0.30)) {
          
       setState(2)
        
       }
        else if( (num > 0.30) && (num < 0.40)){
        setState(1)
        
        }
        else if( (num > 0.40) && (num < 0.50)){
            setState(0)
            
            }
        else if( (num > 0.50) && (num < 0.60)){
                setState(0)
                
                }
        else if(num > 0.60) {
            setState(4)
        }
        
    
     /* if((num >0.20) && (num <0.20)) {
       length1=length-1
       RewardImage=Object.entries(ItemArray)[length1][1].image
      }
       else if( (num > 0.30) && (num < 0.40)){
        length1=length-2
        RewardImage=Object.entries(ItemArray)[length1][1].image
       }
       else if(num <0.40){

       }
       else if(num <0.50){

       }
       else if(num<0.60) {

       }*/
       
       /*console.log(count)
       console.log(parseFloat(hello))
       count++
      add=add+parseFloat(hello)
       console.log(add)*/
   
})

function refresh() {
    document.getElementsByClassName("ball")[0].style.backgroundImage="none"
    document.getElementsByTagName("button")[0].style.visibility="hidden"
   window.location.reload(true)

}

    
    return(
        <>
        <div className='card'>
            <span className='confetti'></span>
    <h1>{config.settings.title}</h1>
    <h3>{config.settings.description}</h3>
    <h2>{label}</h2>
    <button onClick={refresh}><i className="fas fa-redo-alt"></i></button>
    <Ball RewardImage={RewardImage} />
    
</div>
</>
    )
}